import {UserRepository} from './UserRepository.js';
import {Application} from './Application.js';


export class Launcher {
    constructor(express, port){
            const userRepository = new UserRepository();
            this.application = new Application(express, userRepository);
            this.application.start(port);
    }
}
