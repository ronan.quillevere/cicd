import * as ROUTE from './Routes.js';

export class Application {
    constructor(express, userRepository){
            this.express = express;
            this.app = this.express();
            this._bindStatic('static');
            this._bindUsers(userRepository);
    }

    _bindStatic(staticPath){
        this.app.use(this.express.static(staticPath));
    }

    _bindUsers(userRepository){
        this.app.get(ROUTE.USERS, (req, res) => {
            res.json(userRepository.getUsers());
        });
    }

    start(port){
        this.app.listen(port);
    }
}
